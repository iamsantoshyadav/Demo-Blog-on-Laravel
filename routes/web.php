<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
});
/*Route::get('/user/{}',function($id){
    return "This is ".$id."here";
});*/
Route::get("/index",function (){
    return view("pages.index");
});
Route::get("/about","PageController@about");
Route::get("/services","PageController@services");
Route::resource("post","PostController");
Route::resource("/amitabh-bachchan","amitabhPostController");
Route::resource("/todotasks","tasksController");
Route::get('/dashboard','PostController@dashboard');

Auth::routes();

Route::get('/register', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
