<nav class="navbar navbar-inverse">
  <div class="container-fluid">
      <div class="navbar-header">

          <!-- Collapsed Hamburger -->
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
              <span class="sr-only">Toggle Navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>

          <!-- Branding Image -->
          <a class="navbar-brand" href="/">Demo Blog</a>
      </div>

      <div class="collapse navbar-collapse" id="app-navbar-collapse">
          <!-- Left Side Of Navbar -->
          <ul class="nav navbar-nav">
              &nbsp;
          </ul>

          <ul class="nav navbar-nav">
              <li class="active"><a href="/">Home</a></li>
              <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">My Assignments
              <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Registration form (JS)</a></li>
                <li><a href="http://localhost/Demo-Website/registration.php">Registration and Login(PHP & Mysql )</a></li>
                <li><a href="http://localhost/Demo-Website/todaysTask.php"> To Do App</a></li>
                <li><a href="">To Do App (API)</a></li>
                <li><a href="/todotasks">To Do App (Laraval)</a></li>
                <li><a href="/post">Blog with Laraval</a></li>
                <li><a href="/amitabh-bachchan"target='_blank'>Abitabh Bachchan Web Page + Blog</a></li>
              </ul>
            </li>
            <li><a href="#">About</a></li>
            <li><a href="/post">Blog</a></li>
          </ul>

          <!-- Right Side Of Navbar -->
          <ul class="nav navbar-nav navbar-right">
              <!-- Authentication Links -->
              @guest
                  <li><a href="{{ route('login') }}"><span class="glyphicon glyphicon-user"></span> Login</a></li>
                  <li><a href="{{ route('register') }}"><span class="glyphicon glyphicon-log-in"> Register</a></li>
              @else
                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                          <span style='text-transform: uppercase'>{{ Auth::user()->name }}</span><span class="caret"></span>
                      </a>

                      <ul class="dropdown-menu">
                          <li>
                            <a href="/dashboard">Dashboard</a>
                            <a href="/todotasks">To Do App</a>
                              <a href="{{ route('logout') }}"
                                  onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                                  Logout
                              </a>

                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  {{ csrf_field() }}
                              </form>
                          </li>
                      </ul>
                  </li>
              @endguest
          </ul>
      </div>
  </div>
</nav>