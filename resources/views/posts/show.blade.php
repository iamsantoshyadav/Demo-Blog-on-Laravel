@extends("layout.app")
@section("title")
    <title>Show posts</title>
@endsection()
@section("content")
    <a href="/post" class="btn btn-default">Go Back</a>
    <h2>{!!$allPosts->title!!}</h2>
    <img style="width: 100%;height: 50%;"src="/storage/coverImage/{{$allPosts->coverImage}}" alt=""><br><br>
    <div>
        {!!$allPosts->body!!}
    </div>
    @if(!Auth::guest())
    <a href="/post/{{$allPosts->id}}/edit"class="btn btn-default">Edit</a>
    {!!Form::open(['action'=>['PostController@destroy',$allPosts->id],'method'=>'post','class'=>'pull-right'])!!}
      {{Form::hidden('_method','DELETE')}}
      {{Form::submit('Delete',['class'=>'btn btn-danger'])}}
    {!!Form::close()!!}
    @endif

    <hr>
    <small>Written on : {{$allPosts->created_at}} <span class="pull-right">By : {{$user->name}}</span> </small>
@endsection()