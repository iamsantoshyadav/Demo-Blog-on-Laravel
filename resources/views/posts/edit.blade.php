@extends("layout.app")
@section("title")
    <title>form section</title>
@endsection
@section("content")
    <h1>Edit Post</h1>
    {!! Form::open(['action'=>['PostController@update',$allPosts->id],'method'=>'post']) !!}
    <div class="form-group">
        {{Form::label('title','Title')}}
        {{Form::text('title',$allPosts->title,['placeholder'=>'Enter title','class'=>'form-control'])}}
    </div>
    <div class="form-group">
        {{Form::label('body','Body')}}
        {{Form::textarea('body',$allPosts->body,['id'=>'article-ckeditor','placeholder'=>'Body','class'=>'form-control'])}}
    </div>
    <div class="form-group">
        {{Form::hidden('_method','PUT')}}
        {{Form::submit('Submit',['class'=>'btn btn-primary'])}}
    </div>
    {!! Form::close() !!}
@endsection