@extends("layout.app")
@section("title")
<title>index-section-of-the-blog</title>
@endsection
@section("content")
    <h2>Posts</h2>
    @if(count($allPosts)>0)
            @foreach($allPosts as $post)
            <div class="well">
                <div class='row'>
                    <div class='col-md-4 col-sm-4'>
                        <img style="width: 100%;"src="/storage/coverImage/{{$post->coverImage}}" alt="">
                    </div>
                    <div class='col-md-8 col-sm-8'>
                        <h3><a href="/post/{{$post->id}}">{{$post->title}}</a></h3>
                        <p class='text-muted'>{!!filter_var(str_limit($post->body,$limit=200),FILTER_SANITIZE_STRING)!!} <br><a href="/post/{{$post->id}}"> Read More</a></p><br>
                        <hr>
                        <small>Last Updated on : {{$post->updated_at}}</small>
                    </div>
                </div>
            </div>
            {{$allPosts->links()}}
            
            @endforeach
        
    @else
        <p>No post Found</p>
    @endif
@endsection()