@extends("layout.app")
@section("title")
    <title>form section</title>
@endsection
@section("content")
    <h1>Create Post</h1>
    {!! Form::open(['action'=>'PostController@store','method'=>'post','enctype'=>'multipart/form-data']) !!}
    <div class="form-group">
        {{Form::label('title','Title')}}
        {{Form::text('title','',['placeholder'=>'Enter title','class'=>'form-control'])}}
    </div>
    <div class="form-group">
        {{Form::label('body','Body')}}
        {{Form::textarea('body','',['id'=>'article-ckeditor','placeholder'=>'Body','class'=>'form-control'])}}
    </div>
    <div class="form-group">
            {{Form::file('coverImage')}}
        </div>
    <div class="form-group">
        {{Form::submit('Submit',['class'=>'btn btn-primary'])}}
    </div>
   
    {!! Form::close() !!}
@endsection