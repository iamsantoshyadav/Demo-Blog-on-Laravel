@extends('layout.app')

@section('content')
<div class="container">
                <h1 class='text-center'>Dashboard</h1>
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
    <h2>Recent Post</h2>
    @if(count($posts)>0)
        <table class='table table-striped'>
            <tr>
                <th>SN.</th>
                <th>Post Title</th>
                <th></th>
                <th></th>
                <?php $i=1;?>
            </tr>
            @foreach($posts as $post)

                <tr>
                    <td><?php echo $i;$i++;?></td>
                    <td><a href="/post/{{$post->id}}">{{$post->title}}</a></td>
                    <td><a href="/post/{{$post->id}}/edit" class='btn btn-default'>Edit</a></td>
                    <td>
                        {!!Form::open(['action'=>['PostController@destroy',$post->id],'method'=>'post','class'=>'pull-right'])!!}
                            {{Form::hidden('_method','DELETE')}}
                            {{Form::submit('Delete',['class'=>'btn btn-danger'])}}
                          {!!Form::close()!!}
                    </td>
                </tr>
            @endforeach
        </table>
    @else
        <span>You have not created any posts yet</span>
    @endif
    <a href="/post/create" class='btn btn-primary'>Create Post</a><hr>
    
</div>
@endsection
