@extends('layout.app')
@section('content')
<div clsaa='container'>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Pending Tasks</b>  <span class="pull-right"><i class="fa fa-plus-square"></i></span></div>
                <div class="panel-body">
                    @if(count($tasks)>0)
                        <?php $i=1;?>
                        <!--GETING ALL THE TASKS ONE BY ONE-->
                        @foreach($tasks as $task)
                        @if(strtotime($task->due_date)<strtotime(date('Y-m-d')))
                            <?php global $dateErr,$color,$button;
                            $color='#F9919D';
                            $dateErr='Date is passed away ';
                            $button='Delete';
                            ?>
                            
                        @else 
                            <?php global $dateErr,$color,$button;
                            $dateErr='';
                            $color='';
                            $button='Compelete';
                            ?>
                        @endif
                        <div class='well'style ='text-transform: uppercase; background-color: <?php global $color;echo $color?>;'>
                            <div class='row'>
                                <div class='col-md-8 col-sm-8'>
                                <small class='text-muted'><?php global $dateErr; echo $dateErr;?></small><br>
                                    <span><b>{{$i++}}.&emsp14; {{$task->title}} &emsp14;<i class="fa fa-angle-double-down text-primary" aria-hidden="true"data-toggle="collapse" data-target="#{{$task->id}}"></i></b></span>
                                </div>
                                <div class='col-md-2 col-sm-2 pull-right'>
                                    {!!Form::open(['action'=>['tasksController@destroy',$task->id],'method'=>'post','class'=>'pull-right'])!!}
                                        {{Form::hidden('_method','DELETE')}}
                                        {{Form::submit('Compelete',['class'=>'btn btn-primary'])}}
                                      {!!Form::close()!!}
                                </div>
                            </div>
                            <div class 'well'>
                                <div id='{{$task->id}}'class="collapse">
                                    <b class='text-capitalize text-primary'>Description : </b><span class='text-black-50 text-capitalize'>{!!filter_var($task->description, FILTER_SANITIZE_STRING)!!}</span>
                                </div>
                            </div>
                            
                        </div>
                        @endforeach

                    @else
                        <b class='text-center'>You dont have any task </b>
                    @endif
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Create New Task</button><hr>
            <div id="demo" class="collapse">
                <div class="panel panel-default">
                    <div class="panel-heading"><b>Create Tasks</b>  <span class="pull-right"><i class="fa fa-plus-square" aria-hidden="true"></i></i></span></div>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        {{Form::open(['action'=>'tasksController@store','method'=>'post'])}}
                        <div class="form-group">
                            {{Form::label('title','Title')}}
                            {{Form::text('title','',['placeholder'=>'Enter your task title...','class'=>'form-control'])}} <br>
                        </div>
                        <div class="form-group">
                            {{Form::label('description','Description')}}
                            {{Form::textarea('description','',['id'=>'article-ckeditor','placeholder'=>'Enter Task Description here','class'=>'form-control'])}} <br>
                        </div>
                        <div class="form-group">
                                {{Form::label('taskDate','Task date')}}
                                {{Form::date('taskDate','',['class'=>'form-control form_datetime'])}} <br>
                            </div>
                        <div class="form-group">
                                {{Form::submit('Add This Task',['class'=>'btn btn-primary'])}}
                            </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection