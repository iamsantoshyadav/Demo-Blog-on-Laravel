
<!DOCOTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @yield('title')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style>
    .bg-1 {
        background-color: #1abc9c; /* Green */
        color: #ffffff;
    }
    .bg-2 {
        background-color: #474e5d; /* Dark Blue */
        color: #ffffff;
    }
    .bg-3 {
        background-color: #ffffff; /* White */
        color: #555555;
    }
    </style>
    
</head>
<body>
    @include("include.navbar")
    @yield('header')
    <div class="container">
            @include('include.messages')
        
            @yield("content")
    </div>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'article-ckeditor' );
    </script>
    
</body>
</html>