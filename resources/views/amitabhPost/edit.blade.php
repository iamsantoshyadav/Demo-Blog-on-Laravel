@extends('layout.app')
@section('title')
    <title>Edit Your post</title>
@endsection
@section('content')
    <h1>Edit Your Post</h1>
    <div class='container'>
            {!! Form::open(['action'=>['amitabhPostController@update',$posts->id],'method'=>'post']) !!}
        <div class='form-group'>
            {{Form::label('title','Title')}}
            {{Form::text('title',$posts->title,['placeholder'=>'Enter Your Title Here.....','class'=>'form-control'])}}<br>
        </div>
       <div class='form-group'>
            {{Form::label('body','Content')}}
            {{Form::textarea('body',$posts->body,['id'=>'article-ckeditor','placeholder'=>'Enter Your content here.....','class'=>'form-control'])}}    
       </div>
        <div class='form-group'>
            {{Form::hidden('_method','PUT')}}
            {{Form::submit('Submit',['class'=>'btn btn-primary'])}}
        </div>
        
        {!!Form::close()!!} 
    </div>
    
@endsection