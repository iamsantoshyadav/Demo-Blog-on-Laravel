@extends('layout.app')
@section('title')
    <title>Create post</title>
@endsection
@section('content')
    <h1>Create Post</h1>
    <div class='container'>
            {!! Form::open(['action'=>'amitabhPostController@store','method'=>'post','enctype'=>'multipart/form-data']) !!}
        <div class='form-group'>
            {{Form::label('title','Title')}}
            {{Form::text('title','',['placeholder'=>'Enter Your Title Here.....','class'=>'form-control'])}}<br>
        </div>
        <div class='form-group'>
            {{Form::label('body','Content')}}
            {{Form::textarea('body','',['id'=>'article-ckeditor','placeholder'=>'Enter Your content here.....','class'=>'form-control'])}}    
        </div>
        <div class='form-group'>
            {{Form::file('coverPhoto')}}
        </div>
        <div class='form-group'>
            {{Form::submit('Submit',['class'=>'btn btn-primary'])}}
        </div>
        
        {!!Form::close()!!} 
    </div>
    
@endsection