@extends('layout.app')
@section('title')
    <title>Amitabh Bachchan </title>
@endsection
@section('content')
@section('header')
<div class="container-fluid bg-1 text-center">
    <br>
    <img src="/storage/amitabhPage/amitabh.jpg" alt="Lights" style="width:15%"class='img-circle'>
    <h3>Amitabh Bachchan Blog</h3><br>
  </div>
@endsection
<h2>Recently Updated Posts</h2>
@if(!auth::guest())
    <a href="/amitabh-bachchan/create" class='btn btn-info pull-right'><span class='glyphicon glyphicon-pencil'></span>Create New Post</a><br><br>
@endif

@if(count($posts)>0)
        @foreach($posts as $post)
        <div class="well">
            <div class='row'>
                <div class='col-md-4 col-sm-4'>
                    <img style="width: 100%;"src="/storage/amitabhPostImage/{{$post->coverPhoto}}" alt="">
                </div>
                <div class='col-md-8 col-sm-8'>
                    <h3><a href="/amitabh-bachchan/{{$post->id}}">{{$post->title}}</a></h3>
                    <p >{!!filter_var(str_limit($post->body,$limit=200),FILTER_SANITIZE_STRING)!!} <br><a href="/amitabh-bachchan/{{$post->id}}"> Read More</a></p><br>
                    <hr>
                    <small>Last Updated on : {{$post->updated_at}}</small>
                </div>
            </div>
        </div>
      
        {{$posts->links()}}
        @endforeach
        
@else
    <p>No post Found</p>
@endif

</div>
@endsection