@extends('layout.app')
@section('title')
    <title>{{$posts->title}}</title>
@endsection
@section('content')
    <a href="/amitabh-bachchan"class='btn btn-default'>Go Back</a>
    <div class='container'>
       <h1>{{$posts->title}}</h1> 
       <div>
           {!!$posts->body!!}
       </div>
       @if(!auth::guest())
            <a href="/amitabh-bachchan/{{$posts->id}}/edit"class='btn btn-info'><span class="glyphicon glyphicon-edit"></span> Edit</a>
            {!!Form::open(['action'=>['amitabhPostController@destroy',$posts->id],'method'=>'post','class'=>'pull-right'])!!}
                {{Form::hidden('_method','DELETE')}}
                {{Form::submit('Delete',['class'=>'btn btn-danger'])}}
            {!!Form::close()!!}
        @endif
        <hr>
        <small><span class="glyphicon glyphicon-ok-circle"></span>Last Updated on : {{$posts->updated_at}}<span class="pull-right"style='text-transform: uppercase'>By : {{$user->name}}</span></small>
    </div>
@endsection