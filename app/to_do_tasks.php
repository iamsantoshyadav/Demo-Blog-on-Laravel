<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class to_do_tasks extends Model
{
    public function user(){
        return $this->belongsTo('App/User');
    }
}
