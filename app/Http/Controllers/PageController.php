<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index(){
        $title="Index section";
        return view("pages.index")->with('title',$title);
    }
    //for about section 
    public function about(){
        $about="This is about section of the webapp where we are going to add some about section this is some random texts";
        return view("pages.about")->with("about",$about);
    }
    //for services of the app
    public function services(){
        //passing the array datas
        $data=array(
            "title"=>"this is service section",
            "body"=>"in this secttion we are going to provide you no of services available on this webpage",
            "services"=>["api","get","post","update"]
        );
        return view("pages.services")->with($data);
    }
    
}
