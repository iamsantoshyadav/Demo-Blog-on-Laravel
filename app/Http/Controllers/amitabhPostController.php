<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;    
use App\post;
use App\User;
use App\amitabhPost;

class amitabhPostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //for authentication the user 
        $this->middleware('auth',['except'=>['index','show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts=amitabhPost::orderBy('created_at','desc')->paginate(10);
        return view('amitabhPost.index')->with('posts',$posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('amitabhPost.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required',
            'body'=>'required',
            'coverPhoto'=>'image|nullable|max:1999'
        ]);
        //get file name of the image file
        if($request->hasfile('coverPhoto')){
            $fileWithExt=$request->file('coverPhoto')->getClientOriginalName();
            $fileName=pathinfo($fileWithExt,PATHINFO_FILENAME);
            $fileExtension=$request->file('coverPhoto')->getClientOriginalExtension();
            $fileNameToBeStore=$fileName.'_'.time().'.'.$fileExtension;
            $path=$request->file('coverPhoto')->storeAs('public/amitabhPostImage',$fileNameToBeStore);
        }
        else{
            $fileNameToBeStore='noimage.jpg';
        }
        $post=new amitabhPost;
        $post->title=$request->input('title');
        $post->body=$request->input('body');
        $post->user_id=auth()->user()->id;
        $post->coverPhoto=$fileNameToBeStore;
        $post->save();
        return redirect('/amitabh-bachchan')->with('success','Post Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $posts=amitabhPost::find($id);
        $user_id=$posts->user_id;
        $user=User::find($user_id);
        return view('amitabhPost.show')->with(['posts'=>$posts,'user'=>$user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $posts=amitabhPost::find($id);
        $user_id=$posts->user_id;
        if($user_id!=auth()->user()->id){
            return redirect('/amitabh-bachchan')->with('error','Unauthorized page');
        }else{
            return view('amitabhPost.edit')->with('posts',$posts);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'=>'required',
            'body'=>'required',
        ]);
        $post= amitabhPost::find($id);
        $post->title=$request->input('title');
        $post->body=$request->input('body');
        $post->user_id=auth()->user()->id;
        $post->coverPhoto='noimage.jpg';
        $post->save();
        return redirect('/amitabh-bachchan')->with('success','Post Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post=amitabhPost::find($id);
        $user_id=$post->user_id;
        if($user_id!=auth()->user()->id){
            return redirect('/amitabh-bachchan')->with('error','Unauthorized page');
        }else{
            $post->delete();
            return redirect('/amitabh-bachchan')->with('success','Post Deleated Successfully');
        }
        
    }
}
