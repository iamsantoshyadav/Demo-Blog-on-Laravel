<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\post;
use App\User;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //for authentication the user 
        $this->middleware('auth',['except'=>['index','show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allPosts=post::orderBy('created_at','desc')->paginate(10);
        return view("posts.index")->with('allPosts',$allPosts);
       
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dashboard(){
        $user_id=auth()->user()->id;
        $user=User::find($user_id);
        $posts= $user->posts;
        return view('posts.dashboard')->with('posts',$posts);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required',
            'body'=>'required',
            'coverImage'=>'image|nullable|max:1999'
        ]);
        //here we are going to handle the image file
        if($request->hasFile('coverImage')){
            //getting the file name from the image has to be upload
            $fileNameWithExt=$request->file('coverImage')->getClientOriginalName();
            //file extension
            $filename=pathinfo($fileNameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('coverImage')->getClientOriginalExtension();
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            $path=$request->file('coverImage')->storeAs('public/coverImage',$fileNameToStore);

        }else{
            $fileNameToStore='noimage.jpg';
        }
        $post= new post;
        $post->title=$request->input('title');
        $post->body=$request->input('body');
        $post->user_id=auth()->user()->id;
        $post->coverImage=$fileNameToStore;
        $post->save();
        return redirect('/post')->with('success','Post created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $allPosts=post::find($id);
        $user_id=$allPosts->user_id;
        $user=User::find($user_id);
        return view('posts.show')->with(['allPosts'=>$allPosts,'user'=>$user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allPosts=post::find($id);
        if(auth()->user()->id !=$allPosts->user_id){
            return redirect('/post')->with('error','Unauthorized page');    
        }
        return view('posts.edit')->with('allPosts',$allPosts);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required',
            'body'=>'required'
        ]);
        $post= post::find($id);
        $post->title=$request->input('title');
        $post->body=$request->input('body');
        $post->save();
        return redirect('/post')->with('success','Post Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post=post::find($id);
        if(auth()->user()->id !=$post->user_id){
            return redirect('/post')->with('error','Unauthorized page');    
        }
        $post->delete();
        return redirect('/post')->with('success','Post Deleated');

    }
}
